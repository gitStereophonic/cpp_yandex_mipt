#include <string>
#include <sstream>

void EnsureEqual(const std::string& left, const std::string& right) {
  if (left != right) {
    std::stringstream ss;
    ss << left << " != " << right;
    throw std::runtime_error(ss.str());
  }
}
