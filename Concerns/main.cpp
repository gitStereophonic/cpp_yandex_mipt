#include <iostream>
#include <string>
#include <vector>

using namespace std;

void Add(vector<vector<string>>& v, size_t i, string s) {
  v.at(i).push_back(s);
}

void Next(size_t& m, const vector<size_t> days,
          vector<vector<string>>& concern) {
  if (m == 11) m = 0;
  else m++;
  if (days[m] >= concern.size()) {
    concern.resize(days[m]);
  } else {
    for (size_t i = concern.size() - 1; i >= days[m]; i-- ) {
      concern[days[m] - 1].insert(end(concern[days[m] - 1]),
          begin(concern[i]), end(concern[i]));
    }
    concern.resize(days[m]);
  }
}

void Dump(size_t n, const vector<vector<string>>& concern) {
  size_t size = concern.at(n).size();
  cout << size;
  for (size_t i = 0; i < size; i++) {
    cout << " " << concern.at(n).at(i);
  }
  cout << endl;
}

int main()
{
  size_t m = 0;
  const vector<size_t> days = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  vector<vector<string>> concerns(days[m]);

  int opers;
  cin >> opers;

  while (opers > 0) {
    string op;
    cin >> op;
    if (op == "NEXT") {
      Next(m, days, concerns);
    }
    if (op == "ADD") {
      size_t i;
      string deal;
      cin >> i >> deal;
      Add(concerns, --i, deal);
    }
    if (op == "DUMP") {
      size_t i;
      cin >> i;
      Dump(--i, concerns);
    }

    opers--;
  }

  return 0;
}
