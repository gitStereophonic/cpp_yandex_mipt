#include <iostream>

using namespace std;

int main()
{
  float n, a ,b, x, y, result;
  cin >> n >> a >> b >> x >> y;

  if (n > b) {
    result = n - n * (y / static_cast<float>(100.));
  } else if (n > a){
    result = n - n * (x / static_cast<float>(100.));
  } else {
    result = n;
  }

  cout << result;

  return 0;
}
