#ifndef UPDATE_IF_GREATER_H
#define UPDATE_IF_GREATER_H

void UpdateIfGreater(const int first, int& second);

#endif // UPDATE_IF_GREATER_H
