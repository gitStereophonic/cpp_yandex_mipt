#include <iostream>

#include "update_if_greater.h"

using namespace std;

int main()
{
  int a, b;
  cin >> a >> b;

  UpdateIfGreater(a, b);

  cout << "a: " << a << ", b: " << b << endl;

  return 0;
}
