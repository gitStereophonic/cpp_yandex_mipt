#include <vector>

class FunctionPart {
public:
  FunctionPart(char o, double v) {
    operation = o;
    value = v;
  }

  double Apply(double source_value) const {
    if (operation == '+') {
      return source_value + value;
    } else {
      return source_value - value;
    }
  }

  void Invert() {
    if (operation == '+') {
      operation = '-';
    } else {
      operation = '+';
    }
  }

private:
  char operation;
  double value;
};

class Function {
public:
  void AddPart(char o, double v) {
    parts.push_back({o, v});
  }

  double Apply(double w) const {
    for (const auto& part : parts) {
      w = part.Apply(w);
    }
    return w;
  }
  void Invert() {
    for (auto& part : parts) {
      part.Invert();
    }
    reverse(parts.begin(), parts.end());
  }

private:
  std::vector<FunctionPart> parts;
};
