#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main()
{
  vector<int> v;
  int n, x;
  cin >> n;

  while (n > 0) {
    x = static_cast<int>(log2(n));
    v.push_back(x);
    n -= pow(2, x);
  }

  int v_i = 0;
  for (int j = v[0]; j >= 0; j--) {
    if (v_i >= v.size())
      cout << 0;
    else if (j == v[v_i]) {
      cout << 1;
      v_i++;
    } else
      cout << 0;
  }
  return 0;
}
