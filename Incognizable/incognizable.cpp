class Incognizable {
public:
  Incognizable() {}
  Incognizable(int a) {
    x = a;
  }
  Incognizable(int a, int b) {
    x = a;
    y = b;
  }

private:
  int x;
  int y;
};
