#include <iostream>
#include <map>
#include <set>
#include <string>

using namespace std;
//==============================================================================
void Add(const string word1, const string word2,
         map<string, set<string>>& synonyms) {
  synonyms[word1].insert(word2);
  synonyms[word2].insert(word1);
}
//==============================================================================
void Count(const string word, const map<string, set<string>>& synonyms) {
  if (synonyms.count(word) == 0)
    cout << 0 << endl;
  else
    cout << synonyms.at(word).size() << endl;
}
//==============================================================================
void Check(const string word1, const string word2,
           const map<string, set<string>>& synonyms) {
  if (synonyms.count(word1) == 0 || synonyms.count(word2) == 0) {
    cout << "NO" << endl;
  } else {
    if (synonyms.at(word1).count(word2) == 0)
      cout << "NO" << endl;
    else
      cout << "YES" << endl;
  }
}
//==============================================================================
int main()
{
  map<string, set<string>> synonyms;

  size_t q;
  cin >> q;

  while (q > 0) {
    q--;
    string command;
    cin >> command;
    if (command == "ADD") {
      string word1, word2;
      cin >> word1 >> word2;
      Add(word1, word2, synonyms);
    }
    if (command == "COUNT") {
      string word;
      cin >> word;
      Count(word, synonyms);
    }
    if (command == "CHECK") {
      string word1, word2;
      cin >> word1 >> word2;
      Check(word1, word2, synonyms);
    }
  }

  return 0;
}
