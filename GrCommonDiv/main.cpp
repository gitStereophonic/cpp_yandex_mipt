#include <iostream>

using namespace std;

int main()
{
  int a, b, x;
  cin >> a >> b;

  if (a < b) {
    if (b - a > a)
      x = a;
    else
      x = b - a;
  }
  else if (a > b){
    if (a - b > b)
      x = b;
    else
      x = a - b;
  } else {
    cout << a;
    return 0;
  }

  while (x > 0) {
    if (a % x == 0) {
      if (b % x == 0) {
        cout << x;
        return 0;
      }
    }
    x--;
  }

  cout << 0;

  return 0;
}
