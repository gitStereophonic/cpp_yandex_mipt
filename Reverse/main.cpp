#include <iostream>
#include <vector>

#include "reverse.h"

using namespace std;

int main()
{
  vector<int> v = {1, 5, 3, 4, 2};

  cout << "Old vector: ";
  for (auto i : v) {
    cout << i << " ";
  }
  cout << endl;

  Reverse(v);

  cout << "New vector: ";
  for (auto i : v) {
    cout << i << " ";
  }
  cout << endl;

  return 0;
}
