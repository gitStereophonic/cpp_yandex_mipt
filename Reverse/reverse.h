#ifndef REVERSE_H
#define REVERSE_H

#include <vector>

void Reverse(std::vector<int>& v);

#endif // REVERSE_H
