#include <vector>

using namespace std;

void Reverse(vector<int>& v) {
  size_t sz = v.size();
  for (size_t i = 0; i < sz / 2; i ++) {
    auto tmp = v[i];
    v[i] = v[sz - 1 - i];
    v[sz - 1 - i] = tmp;
  }
}
