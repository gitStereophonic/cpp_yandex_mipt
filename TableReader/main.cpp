#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
  string path = "input.txt";
  ifstream input(path);
  int n = 0, m = 0;
  if (input.is_open()) {
    input >> n; input.ignore(1);
    input >> m; input.ignore(1);
    while(n > 0) {
      n--;
      for (int i = m; i > 0;) {
        i--;
        int v = 0;
        input >> v; input.ignore(1);
        cout << setw(10) << v;
        if (i > 0) cout << ' ';
      }
      cout << endl;
    }
  }

  return 0;
}
