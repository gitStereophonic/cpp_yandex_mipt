#include <iostream>
#include <set>
#include <string>

using namespace std;

int main()
{
  set<string> unique;
  size_t n;
  cin >> n;

  while (n > 0) {
    n--;
    string s;
    cin >> s;

    unique.insert(s);
  }

  cout << unique.size() << endl;

  return 0;
}
