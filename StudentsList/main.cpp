#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct Student {
public:
  Student(const string& first, const string& second, string day, string month, string year) {
    first_name = first;
    second_name = second;
    b_day = day;
    b_month = month;
    b_year = year;
  }

  void getName() const {
    cout << first_name << ' ' << second_name << endl;
  }

  void getDate() const {
    cout << b_day << '.' << b_month << '.' << b_year << endl;
  }

private:
  string first_name;
  string second_name;
  string b_day;
  string b_month;
  string b_year;
};

int main()
{
  vector<Student> students;
  int n = 0, m = 0;
  cin >> n;
  while (n > 0) {
    n--;
    string first_name = "";
    string second_name = "";
    string day = "", month = "", year = "";
    cin >> first_name >> second_name >> day >> month >> year;
    students.push_back({first_name, second_name, day, month, year});
  }

  cin >> m;
  while (m > 0) {
    m--;
    string request = "";
    int index = 0;
    cin >> request >> index;
    if (index - 1 >= students.size()) {
      cout << "bad request" << endl;
      continue;
    }
    if (request == "name") {
      students[index - 1].getName();
    } else if (request == "date") {
      students[index - 1].getDate();
    } else {
      cout << "bad request" << endl;
    }
  }

  return 0;
}
