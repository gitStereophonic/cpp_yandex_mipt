#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

int main()
{
  string path = "input.txt";
  ifstream input(path);

  while(input) {
    double val = 0;
    input >> val;
    input.ignore(1);
    cout << fixed << setprecision(3) << val << endl;
  }
  return 0;
}
