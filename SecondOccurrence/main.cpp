#include <iostream>
#include <string>

using namespace std;

int main()
{
  char to_check = 'f';
  string s;
  int i = 0;
  cin >> s;

  for (unsigned long j = 0; j < s.size(); j++) {
    if (s[j] == to_check)
      i++;
    if (i == 2){
      cout << j;
      return 0;
    }
  }

  if (i == 0)
    cout << -2;
  if (i == 1)
    cout << -1;

  return 0;
}
