#include <iostream>

using namespace std;

int main()
{
  int a, b;
  cin >> a >> b;

  if (a % 2 == 1)
    a++;

  for (; a <= b; a += 2) {
    cout << a << " ";
  }

  return 0;
}
