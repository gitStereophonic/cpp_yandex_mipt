#include <iostream>
#include <map>
#include <string>
//==============================================================================
using namespace std;
//==============================================================================
class Person {
public:
  void ChangeFirstName(int year, const string& first_name) {
    // добавить факт изменения имени на first_name в год year
    first[year] = first_name;
  }
//==============================================================================
  void ChangeLastName(int year, const string& last_name) {
    // добавить факт изменения фамилии на last_name в год year
    last[year] = last_name;
  }
//==============================================================================
  string GetFullName(int year) {
    // получить имя и фамилию по состоянию на конец года year
    int f_i = 0, l_i = 0;
    for (auto i : first) {
      if (i.first <= year)
        f_i = i.first;
      else
        break;
    }
    for (auto i : last) {
      if (i.first <= year)
        l_i = i.first;
      else
        break;
    }

    string f = first.count(f_i) > 0 ? first[f_i] : "",
        l = last.count(l_i) > 0 ? last[l_i] : "",
        result = "Incognito";

    if (f != "" && l != "")
      result = f + " " + l;
    if (f == "" && l != "")
      result = l + " with unknown first name";
    if (f != "" && l == "")
      result = f + " with unknown last name";

    return result;
  }
//==============================================================================
private:
  // приватные поля
  map<int, string> first;
  map<int, string> last;
};
