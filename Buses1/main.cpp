#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;
//==============================================================================
void printVector(const vector<string>& v, string excl = "") {
  for (auto s : v)
    if (s != excl)
    cout << s << " ";
  cout << endl;
}
//==============================================================================
vector<string> busesForStop(string stop, const map<string, vector<string>>& buses,
                  const vector<string>& bQuery) {
  vector<string> result;
  for (const auto& bus : bQuery) {
    if (std::find(buses.at(bus).begin(),
                  buses.at(bus).end(), stop)
        != buses.at(bus).end())
      result.push_back(bus);
  }

  return result;
}
//==============================================================================
void stopsForBus(string bus, const map<string, vector<string>>& buses,
                 const vector<string>& bQuery) {
  if (buses.count(bus) == 0) {
    cout << "No bus" << endl;
    return;
  }

  for (const auto& b : buses.at(bus)) {
    cout << "Stop " << b << ": ";
    auto bss = busesForStop(b, buses, bQuery);

    bss.erase(find(bss.begin(), bss.end(), bus));

    if (bss.empty())
      cout << "no interchange" << endl;
    else
      printVector(bss, b);
  }
}
//==============================================================================
void allBuses(const map<string, vector<string>>& buses) {
  if (buses.empty()) {
    cout << "No buses" << endl;
    return;
  }

  for (const auto& b : buses) {
    cout << "Bus " << b.first << ": ";
    printVector(b.second);
  }
}
//==============================================================================
int main()
{
  size_t q;
  cin >> q;
  map<string, vector<string>> buses;
  vector<string> bQuery;

  while (q > 0) {
    q--;
    string command;
    cin >> command;
    if (command == "NEW_BUS") {
      string bus;
      cin >> bus;
      bQuery.push_back(bus);
      size_t stopCount;
      cin >> stopCount;
      vector<string> stops;
      while (stopCount > 0) {
        stopCount--;
        string stop;
        cin >> stop;
        stops.push_back(stop);
      }
      buses[bus] = stops;
    }
    if (command == "BUSES_FOR_STOP") {
      string stop;
      cin >> stop;
      auto bss = busesForStop(stop, buses, bQuery);
      if (bss.empty())
        cout << "No stop" << endl;
      else {
        printVector(bss);
      }
    }
    if (command == "STOPS_FOR_BUS") {
      string bus;
      cin >> bus;
      stopsForBus(bus, buses, bQuery);
    }
    if (command == "ALL_BUSES") {
      allBuses(buses);
    }
  }

  return 0;
}
