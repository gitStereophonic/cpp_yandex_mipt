#include <iostream>
#include <map>
#include <string>

using namespace std;

map<char, int> BuildCharCounters(const string& w) {
  map<char, int> result;
  for (const auto& l : w) {
    result[l]++;
  }

  return result;
}

int main()
{
  int n;
  cin >> n;
  while (n > 0) {
    n--;
    string first, second;
    cin >> first >> second;

    if (first.size() != second.size()) {
      cout << "NO" << endl;
      continue;
    }

    if (BuildCharCounters(first) == BuildCharCounters(second))
      cout << "YES" << endl;
    else
      cout << "NO" << endl;
  }

  return 0;
}
