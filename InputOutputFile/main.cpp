#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
  string path = "input.txt";
  string outPath = "output.txt";
  ifstream input(path);
  ofstream output(outPath);

  if(input.is_open()) {
    string line = "";
    while (getline(input, line)) {
      output << line << endl;
    }
  }
  return 0;
}
