#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
  size_t n = 0;
  cin >> n;
  vector<int> vec(n);

  for (auto& x : vec) {
    cin >> x;
  }

  sort(vec.begin(), vec.end(), [](int i, int j) {
    return abs(i) < abs(j);
  });

  for (const auto& o : vec) {
    cout << o << " ";
  }

  return 0;
}
