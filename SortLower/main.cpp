#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main()
{
  size_t n = 0;
  cin >> n;
  vector<string> strs(n);
  for (auto& s : strs) {
    cin >> s;
  }

  sort(begin(strs), end(strs), [](string s1, string s2) {
    for (auto& s : s1) {
      s = static_cast<char>(tolower(s));
    }
    for (auto& s : s2) {
      s = static_cast<char>(tolower(s));
    }


    return s1 < s2;
  });

  for (const auto s : strs) {
    cout << s << " ";
  }

  return 0;
}
