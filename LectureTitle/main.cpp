#include <iostream>
#include <string>

using namespace std;
//==============================================================================
struct Specialization {
  explicit Specialization(const string& s) {
    value = s;
  }

  string value;
};
//==============================================================================
struct Course {
  explicit Course(const string& s) {
    value = s;
  }

  string value;
};
//==============================================================================
struct Week {
  explicit Week(const string& s) {
    value = s;
  }

  string value;
};
//==============================================================================
struct LectureTitle {
  LectureTitle(const Specialization& s, const Course& c, const Week& w) {
    specialization = s.value;
    course = c.value;
    week = w.value;
  }

  string specialization;
  string course;
  string week;
};
//==============================================================================
int main()
{
  LectureTitle title(
      Specialization("C++"),
      Course("White belt"),
      Week("4th")
  );

  return 0;
}
