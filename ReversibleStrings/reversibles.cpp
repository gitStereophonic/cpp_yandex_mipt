#include <algorithm>
#include <string>

class ReversibleString {
public:
  ReversibleString() {}
  ReversibleString(const std::string& s) {
    st = s;
  }

  void Reverse() {
    std::reverse(st.begin(), st.end());
  }

  std::string ToString() const {
    return st;
  }

private:
  std::string st;
};
