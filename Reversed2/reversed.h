#ifndef REVERSED_H
#define REVERSED_H

#include <vector>

std::vector<int> Reversed(const std::vector<int>& v);

#endif // REVERSED_H
