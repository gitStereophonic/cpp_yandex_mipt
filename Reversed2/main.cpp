#include <iostream>
#include <vector>

#include "reversed.h"

using namespace std;

int main()
{
  vector<int> v = {1, 5, 3, 4, 2};

  cout << "Original: ";
  for (auto i : v) {
    cout << i << " ";
  }
  cout << endl;

  auto result = Reversed(v);

  cout << "Copy: ";
  for (auto i : result) {
    cout << i << " ";
  }
  cout << endl;

  return 0;
}
