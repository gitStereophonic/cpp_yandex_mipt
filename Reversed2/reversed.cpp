#include <vector>

using namespace std;

vector<int> Reversed(const vector<int>& v) {

  vector<int> result;

  for (size_t i = v.size(); i > 0; i--) {
    result.push_back(v[i - 1]);
  }

  return result;
}
