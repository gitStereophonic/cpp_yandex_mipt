#include <vector>
#include <string>

using namespace std;

bool IsPalindrom(string s) {
  size_t len = s.length();
  if (len <= 1)
    return true;

  size_t half = len / 2;
  for (size_t i = 0; i < half; i++) {
    if (s[i] == s[len - 1 - i])
      continue;
    else
      return false;
  }

  return true;
}

vector<string> PalindromFilter(vector<string> words, int minLength) {

  vector<string> filtered;

  for(auto w : words) {
    if (IsPalindrom(w) && w.length() >= static_cast<size_t>(minLength)) {
      filtered.push_back(w);
    }
  }

  return filtered;
}
