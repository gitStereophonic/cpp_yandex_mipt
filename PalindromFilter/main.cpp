#include <iostream>
#include <string>
#include <vector>

#include "palindromfilter.h"

using namespace std;

int main()
{
  vector<string> words = {"weew", "bro", "code"};
  int minLength = 4;

  for (auto p : PalindromFilter(words, minLength)) {
    cout << p << " ";
  }
  cout << endl;

  return 0;
}
