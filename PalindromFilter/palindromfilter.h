#ifndef PALINDROMFILTER_H
#define PALINDROMFILTER_H

#include <string>
#include <vector>

bool IsPalindrom(std::string s);
std::vector<std::string> PalindromFilter(std::vector<std::string> words, int minLength);

#endif // PALINDROMFILTER_H
