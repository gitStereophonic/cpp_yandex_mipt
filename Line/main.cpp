/*
 * Наборы команд:
 *
 * WORRY i: пометить i-го человека с начала очереди (в нумерации с 0) как беспокоящегося;
 * QUIET i: пометить i-го человека как успокоившегося;
 * COME k: добавить k спокойных человек в конец очереди;
 * COME -k: убрать k человек из конца очереди;
 * WORRY_COUNT: узнать количество беспокоящихся людей в очереди.
 *
 */

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int CountWorry(const vector<bool>& w) {
  int count = 0;
  for (auto v : w) {
    if(v) count++;
  }

  return count;
}

void Come(vector<bool>& w, int k) {
  if (k < 0) {
    k *= -1;
    w.resize(w.size() - static_cast<size_t>(k));
  } else
    w.resize(w.size() + static_cast<size_t>(k), false);
}

void Worry(vector<bool>& w, const int i) {
  w[static_cast<size_t>(i)] = true;
}

void Quiet(vector<bool>& w, const int i) {
  w[static_cast<size_t>(i)] = false;
}

void Action(vector<bool>& w, const string a, int v) {
  if (a == "COME")
    Come(w, v);
  if (a == "WORRY")
    Worry(w, v);
  if (a == "QUIET")
    Quiet(w, v);
}

int main()
{
  int n = 0;

  cin >> n;

  vector<bool> worry;

  for (int i = 0; i < n; i++) {
    string s;
    cin >> s;
    if (s != "WORRY_COUNT") {
      int v;
      cin >> v;
      Action(worry, s, v);
    } else
      cout << CountWorry(worry) << endl;
  }

  return 0;
}
