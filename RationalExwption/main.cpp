//==============================================================================
#include <iostream>
#include <sstream>
#include <iomanip>
#include <map>
#include <set>
#include <vector>
#include <exception>
//==============================================================================
using namespace std;
//==============================================================================
class Rational {
public:
  Rational() {
    _numerator = 0;
    _denominator = 1;
  }

  Rational(int numerator, int denominator) {
    if (denominator == 0) {
      throw invalid_argument("Zero den");
    }

    if (numerator == 0) {
      _numerator = numerator;
      _denominator = 1;
      return;
    }

    int n = 1, d = 1;
    if (numerator < 0) {
      n = -1;
      numerator *= n;
    }
    if (denominator < 0) {
      d = -1;
      denominator *= d;
    }

    int nod = numerator;
    int x;

    if (numerator < denominator) {
      if (denominator - numerator > numerator)
        x = numerator;
      else
        x = denominator - numerator;
    }
    else if (numerator > denominator){
      if (numerator - denominator > denominator)
        x = denominator;
      else
        x = numerator - denominator;
    } else {
      _numerator = 1 * d * n;
      _denominator = 1;
      return;
    }

    while (x > 0) {
      if (numerator % x == 0) {
        if (denominator % x == 0) {
          nod = x;
          break;
        }
      }
      x--;
    }

    _numerator = numerator / nod * n * d;
    _denominator = denominator / nod;
  }

  int Numerator() const {
    return _numerator;
  }

  int Denominator() const {
    return _denominator;
  }

private:
  int _numerator;
  int _denominator;
};
//==============================================================================
bool operator==(const Rational& lhs, const Rational& rhs) {
  if (lhs.Numerator() == rhs.Numerator() &&
      lhs.Denominator() == rhs.Denominator())
    return true;
  else
    return false;
}
//==============================================================================
Rational operator+(const Rational& lhs, const Rational& rhs) {
  int den = lhs.Denominator() * rhs.Denominator();
  int num = lhs.Numerator() * rhs.Denominator() +
      rhs.Numerator() * lhs.Denominator();
  return Rational{num, den};
}
//==============================================================================
Rational operator-(const Rational& lhs, const Rational& rhs) {
  int den = lhs.Denominator() * rhs.Denominator();
  int num = lhs.Numerator() * rhs.Denominator() -
      rhs.Numerator() * lhs.Denominator();
  return Rational{num, den};
}
//==============================================================================
Rational operator*(const Rational& lhs, const Rational& rhs) {
  int num = lhs.Numerator() * rhs.Numerator();
  int den = lhs.Denominator() * rhs.Denominator();
  return Rational{num, den};
}
//==============================================================================
Rational operator/(const Rational& lhs, const Rational& rhs) {
  if (rhs.Numerator() == 0) {
    throw domain_error("Div by zero!");
  }
  int num = lhs.Numerator() * rhs.Denominator();
  int den = lhs.Denominator() * rhs.Numerator();
  return Rational{num, den};
}
//==============================================================================
ostream& operator<<(ostream& stream, const Rational& rational) {
  stream << rational.Numerator() << "/" << rational.Denominator();
  return stream;
}
//==============================================================================
istream& operator>>(istream& stream, Rational& rational) {
  int num = rational.Numerator(), den = rational.Denominator();
  stream >> num;
  stream.ignore(1);
  stream >> den;
  rational = Rational{num, den};
  return stream;
}
//==============================================================================
bool operator<(const Rational& lhs, const Rational& rhs) {
  int l = lhs.Numerator() * rhs.Denominator();
  int r = rhs.Numerator() * lhs.Denominator();
  return l < r;
}
//==============================================================================
int main() {
    try {
        Rational r(1, 0);
        cout << "Doesn't throw in case of zero denominator" << endl;
        return 1;
    } catch (invalid_argument&) {
    }

    try {
        auto x = Rational(1, 2) / Rational(0, 1);
        cout << "Doesn't throw in case of division by zero" << endl;
        return 2;
    } catch (domain_error&) {
    }

    cout << "OK" << endl;
    return 0;
}
