#include <string>

using namespace std;

bool IsPalindrom(string s) {
  size_t len = s.length();
  if (len <= 1)
    return true;

  size_t half = len / 2;
  for (size_t i = 0; i < half; i++) {
    if (s[i] == s[len - 1 - i])
      continue;
    else
      return false;
  }

  return true;
}
