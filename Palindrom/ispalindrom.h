#ifndef ISPALINDROM_H
#define ISPALINDROM_H
#include <string>

bool IsPalindrom(std::string s);

#endif // ISPALINDROM_H
