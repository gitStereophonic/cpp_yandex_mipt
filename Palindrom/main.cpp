#include <iostream>
#include <string>
#include <ispalindrom.h>

using namespace std;

int main()
{
  string s;
  cin >> s;
  cout << IsPalindrom(s);
  return 0;
}
