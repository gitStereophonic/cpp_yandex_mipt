#include <iostream>
#include <map>
#include <string>

using namespace std;
//==============================================================================
void ChangeCapital(string country, string newCapital,
                   map<string, string>& countries) {
  if (countries.count(country) == 0) {
    cout << "Introduce new country " << country
         << " with capital " << newCapital << endl;
  } else {
    if (countries[country] == newCapital) {
      cout << "Country " << country
           << " hasn't changed its capital" << endl;
    } else {

      cout << "Country " << country
           << " has changed its capital from "
           << countries[country] << " to " << newCapital
           << endl;
    }
  }

  countries[country] = newCapital;
}
//==============================================================================
void Rename(string oldCountryName, string newCountryName,
            map<string, string>& countries) {
  if (oldCountryName == newCountryName) {
    cout << "Incorrect rename, skip" << endl;
    return;
  }
  if (countries.count(newCountryName) > 0) {
    cout << "Incorrect rename, skip" << endl;
    return;
  }
  if (countries.count(oldCountryName) == 0) {
    cout << "Incorrect rename, skip" << endl;
    return;
  }
  string capital = countries[oldCountryName];
  countries.erase(oldCountryName);
  countries[newCountryName] = capital;

  cout << "Country " << oldCountryName << " with capital "
       << capital << " has been renamed to " << newCountryName << endl;
}
//==============================================================================
void About(string country, const map<string, string>& countries) {
  if(countries.count(country) == 0) {
    cout << "Country " << country << " doesn't exist" << endl;
  } else {
    cout << "Country " << country << " has capital "
         << countries.at(country) << endl;
  }
}
//==============================================================================
void Dump(const map<string, string>& countries) {
  if(countries.size() == 0) {
    cout << "There are no countries in the world" << endl;
  }
  else {
    for (const auto& country : countries) {
      cout << country.first << "/" << country.second << " ";
    }
    cout << endl;
  }
}
//==============================================================================
int main()
{
  size_t q;
  cin >> q;
  map<string, string> countries;

  while (q > 0) {
    q--;

    string request;
    cin >> request;

    if(request == "ABOUT") {
      string country;
      cin >> country;
      About(country, countries);
    }
    if(request == "DUMP") {
      Dump(countries);
    }
    if(request == "RENAME") {
      string oldCountryName, newCountryName;
      cin >> oldCountryName >> newCountryName;
      Rename(oldCountryName, newCountryName, countries);
    }
    if(request == "CHANGE_CAPITAL") {
      string country, newCapital;
      cin >> country >> newCapital;
      ChangeCapital(country, newCapital, countries);
    }
  }

  return 0;
}
