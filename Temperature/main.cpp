#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

void PrintVector(const vector<int>& v) {
  for (size_t i = 0; i < v.size(); i++) {
    cout << v[i];
    if (i != v.size() - 1)
      cout << " ";
  }
}

int Avg(const vector<int>& v) {
  int sum = 0;
  for (auto i : v) {
    sum += i;
  }

  return sum / static_cast<int>(v.size());
}

vector<int> getOverAvg(const vector<int>& v, int avg) {
  vector<int> r;
  for (size_t i = 0; i < v.size(); i++) {
    if (v[i] > avg) {
      r.push_back(static_cast<int>(i));
    }
  }

  return r;
}

int main()
{
  int n;
  cin >> n;
  size_t n_t = static_cast<size_t>(n);

  vector<int> temps(n_t);

  for (size_t i = 0; i < n_t; i++) {
    cin >> temps[i];
  }

  int avg = Avg(temps);

  auto result = getOverAvg(temps, avg);

  cout << result.size() << endl;

  PrintVector(result);

  return 0;
}
