#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;
//==============================================================================
void addBus(const vector<string>& bus, map<vector<string>, int>& buses) {
  if (buses.count(bus) > 0) {
    cout << "Already exists for " << buses[bus] << endl;
  } else {
    int s = static_cast<int>(buses.size() + 1);
    buses[bus] = s;
    cout << "New bus " << s << endl;
  }
}
//==============================================================================
int main()
{
  size_t q;
  cin >> q;
  map<vector<string>, int> buses;

  while (q > 0) {
    q--;
    size_t n;
    cin >> n;
    vector<string> stops;

    while (n > 0) {
      n--;
      string stop;
      cin >> stop;
      stops.push_back(stop);
    }

    addBus(stops, buses);
  }

  return 0;
}
