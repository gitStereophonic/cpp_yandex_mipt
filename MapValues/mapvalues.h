#ifndef MAPVALUES_H
#define MAPVALUES_H

#include <map>
#include <set>
#include <string>

std::set<std::string> BuildMapValuesSet(const std::map<int, std::string>& map);

#endif // MAPVALUES_H
