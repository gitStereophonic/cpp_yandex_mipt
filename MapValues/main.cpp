#include <iostream>
#include "mapvalues.h"

using namespace std;

int main()
{
  map<int, string> map = {
    {1, "odd"}, {2, "even"}, {3, "odd"},
    {4, "even"}, {5, "odd"}};

  set<string> values = BuildMapValuesSet(map);

  for (const auto& v : values) {
    cout << v << endl;
  }

  return 0;
}
