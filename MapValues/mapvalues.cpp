#include <map>
#include <set>
#include <string>

using namespace std;

set<string> BuildMapValuesSet(const map<int, string>& map) {
  set<string> values;
  for (const auto& m : map) {
    values.insert(m.second);
  }

  return values;
}
