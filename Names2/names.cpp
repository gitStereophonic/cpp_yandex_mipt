#include <algorithm>
#include <iostream>
#include <map>
#include <string>
//==============================================================================
using namespace std;
//==============================================================================
class Person {
public:
  void ChangeFirstName(int year, const string& first_name) {
    // добавить факт изменения имени на first_name в год year
    first[year] = first_name;
  }
//==============================================================================
  void ChangeLastName(int year, const string& last_name) {
    // добавить факт изменения фамилии на last_name в год year
    last[year] = last_name;
  }
//==============================================================================
  string GetFullName(int year) {
    // получить имя и фамилию по состоянию на конец года year
    int f_i = 0, l_i = 0;
    for (auto i : first) {
      if (i.first <= year)
        f_i = i.first;
      else
        break;
    }
    for (auto i : last) {
      if (i.first <= year)
        l_i = i.first;
      else
        break;
    }

    string f = first.count(f_i) > 0 ? first[f_i] : "",
        l = last.count(l_i) > 0 ? last[l_i] : "",
        result = "Incognito";

    if (f != "" && l != "")
      result = f + " " + l;
    if (f == "" && l != "")
      result = l + " with unknown first name";
    if (f != "" && l == "")
      result = f + " with unknown last name";

    return result;
  }
//==============================================================================
  string GetFullNameWithHistory(int year) {
    // получить все имена и фамилии по состоянию на конец года year

    map<int, string> old_first, old_last;
    string prev = "";

    int f_i = 0, l_i = 0;
    for (auto i : first) {
      if (i.first <= year) {
        f_i = i.first;
        if (i.second != prev)
          old_first[i.first] = i.second;
        prev = i.second;
      } else
        break;
    }

    prev = "";
    for (auto i : last) {
      if (i.first <= year) {
        l_i = i.first;
        if (i.second != prev)
          old_last[i.first] = i.second;
        prev = i.second;
      } else
        break;
    }

    auto fI = old_first.end();
    if (fI != old_first.begin())
      old_first.erase(--fI);
    auto lI = old_last.end();
    if (lI != old_last.begin())
      old_last.erase(--lI);

    string f = first.count(f_i) > 0 ? first[f_i] : "",
        l = last.count(l_i) > 0 ? last[l_i] : "",
        result = "Incognito";

    if (f != "" && l != "")
      result = f + CreateOld(old_first) + " "
          + l + CreateOld(old_last);
    if (f == "" && l != "")
      result = l + CreateOld(old_last) + " with unknown first name";
    if (f != "" && l == "")
      result = f + CreateOld(old_first) + " with unknown last name";

    return result;
  }
//==============================================================================
private:
  // приватные поля
  string CreateOld(const map<int, string>& old) {
    string s = "";
    if (old.size() > 0) {
      s = " (";
      size_t i = 0;
      for (auto m = old.rbegin(); m != old.rend(); m++) {
        s += m->second;
        if (i < old.size() - 1)
          s += ", ";
        i++;
      }
      s += ")";
    }
    return s;
  }
//==============================================================================
  map<int, string> first;
  map<int, string> last;
};
