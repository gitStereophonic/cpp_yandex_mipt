#include <iostream>
#include <string>
#include <vector>

#include "move_strings.h"

using namespace std;

int main()
{
  vector<string> source = {"a", "b", "c"};
  vector<string> destination = {"z"};

  MoveStrings(source, destination);

  cout << "source: ";
  for (auto w : source) {
    cout << w << " ";
  }
  cout << endl;

  cout << "destination: ";
  for (auto w : destination) {
    cout << w << " ";
  }
  cout << endl;

  return 0;
}
