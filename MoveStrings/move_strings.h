#ifndef MOVE_STRINGS_H
#define MOVE_STRINGS_H

#include <string>
#include <vector>

void MoveStrings(std::vector<std::string>& source,
                 std::vector<std::string>& destination);

#endif // MOVE_STRINGS_H
