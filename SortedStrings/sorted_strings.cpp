#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
//==============================================================================
using namespace std;
//==============================================================================
class SortedStrings {

public:
  void AddString(const string& s) {
    m_Strings.push_back(s);
  }

  vector<string> GetSortedStrings() {
    // получить набор из всех добавленных строк в отсортированном порядке
    sort(m_Strings.begin(), m_Strings.end());
    return m_Strings;
  }

private:
  vector<string> m_Strings;
};
